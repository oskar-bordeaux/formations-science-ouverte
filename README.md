# Formations Science Ouverte

Liste de formations proposées dans le cadre des _[ateliers-BU](https://bibliotheques.u-bordeaux.fr/Se-former/Ateliers-des-BU)_ ou _[kit de survie documentaire](https://bibliotheques.u-bordeaux.fr/Se-former/Kit-de-survie-documentaire-pour-doctorants)_.


